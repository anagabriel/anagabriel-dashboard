module.exports = {
  pwa: {
    name: 'anagabriel - dashboard',
    themeColor: '#000000',
    msTileColor: '#ffffff',
    workboxOptions: {
      directoryIndex: '/',
    },
    iconPaths: {
      favicon32: 'favicon.png',
      favicon16: 'favicon.png',
      appleTouchIcon: 'favicon.png',
      maskIcon: 'favicon.png',
      msTileImage: 'favicon.png'
    }
  }
}
