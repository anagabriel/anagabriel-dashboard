const express = require('express');
const router = express.Router();

const title = 'anagabriel - dashboard';
const description = 'this is a dashboard for my personal website';

const {js, css} = require('../utils/inject').jsCSS();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', {
    title, description, js, css
  });
});

router.get('/practice', (req, res, next) => {
  res.render('index', {
    title, description, js, css
  });
});

router.get('/schedule', (req, res, next) => {
  res.render('index', {
    title, description, js, css
  });
});

router.get('/video', (req, res, next) => {
  res.render('index', {
    title, description, js, css
  });
});

router.get('/profile', (req, res, next) => {
  res.render('index', {
    title, description, js, css
  });
});

module.exports = router;
