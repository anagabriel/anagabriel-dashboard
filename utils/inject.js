function noIndex() {
  const fs = require('fs');
  fs.unlink(`${__dirname}/../dist/index.html`,
    (err) => {
      console.log('index.html was deleted');
    }
  );

  const mani = fs.readdirSync(
    `${__dirname}/../dist/`
  ).filter(
    i => i.indexOf('.js') !== -1 &&
         i.indexOf('precache-manifest') !== -1
  ).map(i => `${__dirname}/../dist/${i}`)[0];

  fs.readFile(mani, 'utf-8', (err, data) => {
    if (err) return console.log('not found');
    const val = data.replace(
      `"/index.html"`, `"/"`
    );

    fs.writeFile(mani, val, 'utf-8', (err) => {
      if (err) return console.log('error');
      console.log('file done');
    });
  });
}

function jsCSS() {
  const fs = require('fs');

  const js = fs.readdirSync(
    `${__dirname}/../dist/js/`
  ).filter(
    i => i.indexOf('.js') !== -1 &&
         i.indexOf('.map') === -1
  ).map(i => `/js/${i}`).reverse();

  const css = fs.readdirSync(
    `${__dirname}/../dist/css/`
  ).filter(
    i => i.indexOf('.css') !== -1 &&
         i.indexOf('.map') === -1
  ).map(i => `/css/${i}`).reverse();

  return {js, css};
}

module.exports = { noIndex, jsCSS }
