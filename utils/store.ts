import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

enum View {
  HOME = 0,
  ERROR = 1,
  VIDEO = 2,
  PROFILE = 3,
  PRACTICE = 4,
  SCHEDULE = 5,
}

interface State {
  user: {
    id?: string,
    login?: string
  },
  view: View
}

// root state object.
// each Vuex instance is just a single state tree.
const state: State = {
  user: {},
  view: View.HOME
}

// mutations are operations that actually mutates the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {
  check (state: State) {
    let id = document.cookie.substring(
      document.cookie.indexOf('id=')
    ); if (id.indexOf(';') !== -1) {
      id = id.substring(0, id.indexOf(';'))
    } id = id.substring(id.indexOf('=') + 1)

    let login = document.cookie.substring(
      document.cookie.indexOf('login=')
    ); if (login.indexOf(';') !== -1) {
      login = login.substring(0, login.indexOf(';'))
    } login = login.substring(login.indexOf('=') + 1)

    if (!id || !login) {
      window.location.href = 'https://anagabriel.me/login'
    } else fetch(`https://anagabriel.me/cred/${id}/${login}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        window.location.href = 'https://anagabriel.me/login'
      } else {
        state.user = res.result;
      }
    }).catch(
      () => console.log('error on cred check')
    );
  },
  home (state: State) {
    state.view = View.HOME
  },
  practice (state: State) {
    state.view = View.PRACTICE
  },
  profile (state: State) {
    state.view = View.PROFILE
  },
  video (state: State) {
    state.view = View.VIDEO
  },
  schedule (state: State) {
    state.view = View.SCHEDULE
  },
  error (state: State) {
    state.view = View.ERROR
  }
}

// actions are functions that cause side effects and can involve
// asynchronous operations.
const actions = {
  home: ({ commit }: {commit: Function}) => commit('home'),
  check: ({ commit }: {commit: Function}) => commit('check'),
  error: ({ commit }: {commit: Function}) => commit('error'),
  video: ({ commit }: {commit: Function}) => commit('video'),
  profile: ({ commit }: {commit: Function}) => commit('profile'),
  practice: ({ commit }: {commit: Function}) => commit('practice'),
  schedule: ({ commit }: {commit: Function}) => commit('schedule')
}

// getters are functions
const getters = {
  user: (state: State) => state.user,
  view: (state: State) => state.view,
  id: (state: State) => state.user.id,
  login: (state: State) => state.user.login,
}

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
