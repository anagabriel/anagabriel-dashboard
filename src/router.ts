import Vue from 'vue'
import Router, {
  Route
} from 'vue-router'
import store from '../utils/store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/schedule',
      name: 'schedule',
      component: () => import('./views/Schedule.vue')
    },
    {
      path: '/practice',
      name: 'practice',
      component: () => import('./views/Practice.vue')
    },
    {
      path: '/video',
      name: 'video',
      component: () => import('./views/Video.vue')
    }
  ]
})

router.beforeEach((to: Route, from: Route, next: Function) => {
  switch (to.name) {
    case 'home':
    case 'video':
    case 'profile':
    case 'practice':
    case 'schedule':
      store.dispatch(to.name)
      break
    default:
      store.dispatch('error')
      break
  } next()
})

export default router
