import Vue from 'vue'

import {
  Form,
  Icon,
  Badge,
  Input,
  Button,
  Layout,
  Upload,
  Calendar
} from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

import App from './App.vue'

import store from '../utils/store'
import router from './router'

import './registerServiceWorker'

const { TextArea } = Input

const {
  Footer,
  Header,
  Content
} = Layout

Vue.component(Form.name, Form)
Vue.component(Icon.name, Icon)
Vue.component(Badge.name, Badge)
Vue.component(Input.name, Input)
Vue.component(Button.name, Button)
Vue.component(Footer.name, Footer)
Vue.component(Header.name, Header)
Vue.component(Layout.name, Layout)
Vue.component(Upload.name, Upload)
Vue.component(Content.name, Content)
Vue.component(Calendar.name, Calendar)
Vue.component(TextArea.name, TextArea)

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
  created: function () {
    store.dispatch('check')
  }
}).$mount('#app')
