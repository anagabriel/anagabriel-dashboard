# anagabriel-dashboard

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run locally
```
npm start
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Dashboard Design
## Purpose
Anagabriel-Dashboard is a project that is centered around the idea that all desires in life can be broken down into achievable goals. There is a science to goal setting.  By using measurable parameters, people can create and attain their dreams with just a click of a mouse.  

The system is going to help users create and set goals for any aspect in their lives whether they are career, intrapersonal, interpersonal or leisure oriented. Users will be able to set schedules, meetings, and reminders related to any goals they have in mind.  


## Design Outline
![sytstem diagrams](.readme/system_diagram.png)

**Client Servers**: hosts the Node.js frontend UI that will be requested by user clients

**User Client**: Vue.js + Node.js + Ant Design will be the user’s main way to interact with the system and implements a Service Worker for a better offline experience

**Image Bucket**: all images for this product will be stored and accessed here

**API Servers**: all API routes will be hosted on these servers and will be the only way users can communicate with the database which will be written in C++

**Database**: this will be a NoSQL database called DynamoDB hosted by AWS

## Design Issues
Each design issue requires a descriptive title, solution options for the issue, and justification of your choice.

### Non-Functional
Issue: What video chat API will be used?
* Option 1: [Vidyo.io](https://developer.vidyo.io/#/documentation/4.1.25.30)
* Option 2: [appear.in](https://developer.appear.in/)
* **Option 3**: [Jitsi](https://jitsi.org/)

I FINALLY HAVE AN ANSWER. Jitsi is open source, so that is the service I am going to use. There’s also an Apache version of it, but it looks like Windows 98 and that’s not the vibe I’m feeling. So, **Option 3** is the one I’m going to use on this project.

Issue: What coding API will be used?
* Option 1: [TopCoder](https://tcapi.docs.apiary.io/)
* Option 2: [HackerRank](https://www.hackerrank.com/api/docs)
* Option 3: personal web crawlers

Decision details

Issue: What job APIs will be used?
* Option 1: [Jopwell](https://api.jopwell.com/api/job_levels?limit=3000&offset=0)
* Option 2: [Indeed](http://opensource.indeedeng.io/api-documentation/docs/job-search/)
* Option 3: [Google](https://cloud.google.com/talent-solution/job-search/docs/apis)
* Option 4: [Glassdoor](https://www.glassdoor.com/developer/jobsApiActions.htm)
* Option 5: [LinkedIn](https://developer.linkedin.com/docs/guide/v2/jobs#)
* Option 6: personal web crawlers

Decision details

Issue: What calendar API will be used?
* Option 1: [Google](https://developers.google.com/calendar/)
* Option 2: [Evernote](https://dev.evernote.com/doc/)
* Option 3: personal apis

Not sure what I think about these, because it requires users to have accounts with them and OAuth which shouldn't be requirements to use my site. Creating my own APIs will require I decent amount of work...

### Functional
Issue:
* Option 1:
* Option 2:
* Option 3:

## Design Details
### Class Diagrams
![class diagram](.readme/class_diagram.png)

### Sequence Diagrams
![sequence diagram](.readme/sequence_diagram.png)

### Mockups
#### Dashboard
![dashboard](.readme/dashboard.png)

#### Profile
![profile](.readme/profile.png)

#### Schedule
![schedule](.readme/schedule.png)

#### Practice
![practice](.readme/practice.png)

#### Video
![video](.readme/video.png)
